const config = require('../config');
const http = require('http');
const server = http.createServer();
const io = require('socket.io')(server, {
  cors: {
    origin: "http://localhost:8080",
    methods: ["GET", "POST"],
    credentials: true // If you need to send cookies
  },
  allowEIO3: true //https://socket.io/docs/v3/troubleshooting-connection-issues/
});
const fs = require('fs');
const path = require('path');
let connected = [];
let chats = [];
let chatsHistory = [];
const port = config.socketPort || 3000;
server.listen(port, () => {
  console.log(`server start at port ${port}`);
});

const getConnectedList = () => {
  const arr = [];
  let level;

  for (let client in io.sockets.connected) {

    if (io.sockets.sockets[client].handshake.headers.referer.includes('/client')) {
      level = 'client';
    }

    if (io.sockets.sockets[client].handshake.headers.referer.includes('/host')) {
      level = 'host';
    }
    arr.push({
      id: client,
      level: level
    });
  }
  return arr;
};

const saveLogFile = (data) => { //https://stackoverflow.com/questions/3459476/how-to-append-to-a-file-in-node/43370201#43370201
  const stream = fs.createWriteStream(data.url, { flags: 'a' });
  stream.write(data.content);
  stream.end();
}

const updateChats = (data, del = false) => {
  const chatId = chats.findIndex((chat) => {
    if (chat.id === data.id) { //chat.id === socket id of client that started
      return chat;
    }
  });

  if (chatId !== -1) { //chat dialog exists, we can delete or update messages
    if (del) {
      chats.splice(chatId, 1);
    } else {
      if (data.level === 'client') {
        chats[chatId].author = data.username;
      }
      chats[chatId].messages.push({
        level: data.level,
        message: data.message,
        username: data.username
      });
      chatsHistory.push({
        chatId: data.id,
        authorIp: data.ip,
        date: Date.now(),
        entry: chats[chatId].messages.slice(-1)[0]

      });
      saveLogFile({
        url: path.join(__dirname, '/logs/log-chat.json'),
        content: JSON.stringify(chatsHistory.slice(-1)[0])
      });
    }
  } else { //fresh dialog, lets register it by author's socket id
    let dialog = {};
    dialog.id = data.id;
    dialog.author = data.username;
    dialog.messages = [];

    if (data.level && data.username && data.message) {
      dialog.messages.push({
        level: data.level,
        username: data.username,
        message: data.message
      });
    }
    chats.push(dialog);
  }
  informSockets();

};
const informSockets = () => {
  for (let client in connected) {
    if (connected[client].level === 'host') {
      io.to(connected[client].id).emit('updateChats', chats);

    } else {
      const chatIdx = chats.findIndex((chat) => {
        if (chat.id === connected[client].id) {
          return chat;
        }
      });
      io.to(connected[client].id).emit('updateChats', chats[chatIdx]);
    }
  }
};

io.on('connection', (socket) => {
  console.log(`${socket.id} connected, ip: ${socket.handshake.address}`);

  connected = getConnectedList();
  if (socket.handshake.headers.referer.includes('/client')) {
    updateChats({ id: socket.id });
  }

  if (socket.handshake.headers.referer.includes('/host')) {
    informSockets();
  }

  socket.on('disconnect', () => {
    console.log(`${socket.id} disconnected, ip: ${socket.handshake.address}`);
    const connectedIdx = connected.findIndex((client) => {
      if (client.id === socket.id) { //chat.id === socket id of client that started
        return client;
      }
    });

    if (connectedIdx !== -1 && connected[connectedIdx].level === 'client') {
      updateChats({ id: socket.id }, true); //lets delete chat created by disconected cocket
    }
    connected = getConnectedList(); //update connected list
  });

  socket.on('client-send-message', response => {
    updateChats({
      ip: socket.handshake.address,
      id: socket.id,
      level: 'client',
      author: response.userName,
      username: response.username,
      message: response.message
    });
    // console.dir(chats);
  });

  socket.on('host-send-message', response => {
    updateChats({
      ip: socket.handshake.address,
      id: response.chatId,
      level: 'host',
      username: response.username,
      message: response.message
    });
  });
});