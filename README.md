# Brick chat

## How to use (for windows)
```
$ cd brick-chat-front
$ npm install
$ cd ../brick-chat-server
$ npm install
$ cd ..
$ npm run winstart
```
Open `http://localhost:8080/client` to chat with host.

Open `http://localhost:8080/host` to chat with all connected clients.

Logfile with all conversations is saved in /brick-chat-server/logs/log-chat.json

To chat with clients on other remote devices, in `config.js` change socketServer property from localhost to ip (for example `http://192.168.1.1`) of computer on which /brick-chat-server service will run. After that, restart both /brick-chat-server and /brick-chat-front.