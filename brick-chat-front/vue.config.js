process.env.VUE_APP_SOCKETPORT = require('../config').socketPort;
process.env.VUE_APP_SOCKETSERVER = require('../config').socketServer;

module.exports = {
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.pug$/,
          loader: 'pug-plain-loader'
        }
      ]
    }
  }
};