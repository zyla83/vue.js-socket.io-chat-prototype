import Vue from 'vue'
import Router from 'vue-router'
import Client from './views/Client.vue'
import Host from './views/Host.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/client'
    },
    {
      path: '/host',
      name: 'host',
      component: Host
    },
    {
      path: '/client',
      name: 'client',
      component: Client,
      props: true
    }
  ]
})
